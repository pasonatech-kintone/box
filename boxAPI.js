jQuery.noConflict();
(function ($) {
	"use strict";
	var logflg = false;
	var APPID_CLIANT = 2;
	var APPID_ANKEN = 4;
	var APPID_APPLICANT = 9;
	var CLIENT_CODE="ram4ixh0z8b8c1pcqwr6jk87c823a2ry";//クライアントコード
	var CLIENT_SECRET="eN4BfAGrpPrCBvpjL7igy385nE6qYbZU";//クライアント機密コード
    var URL="https://account.box.com/api/oauth2/authorize?response_type=code&client_id="+CLIENT_CODE+
    "&redirect_uri=https://visionary.cybozu.com/k/59/";
    function getAccessToken(){
        var token=null;
        var xmlHttp = new XMLHttpRequest();
        var appUrl=kintone.api.url("/k/v1/record")+"?app="+117+'&id=1';
        xmlHttp.open("GET", appUrl, false);
        xmlHttp.setRequestHeader('X-Cybozu-API-Token', "4Jvm6tGkTBUGJKbANliQHgT4zuiHTlzjV5dg78uf");
        xmlHttp.send(null);
        if(xmlHttp.status === 200){
            var resp=JSON.parse(xmlHttp.responseText);
            var refresh_token=resp.record["refresh_token"]["value"];
            //リフレッシュトークンを利用して、新しい認証トークンを取得する
            var body={
                grant_type:"refresh_token",
                refresh_token:refresh_token,
                client_id:CLIENT_CODE,
                client_secret:CLIENT_SECRET
            }
            $.ajax({
                type: 'POST',
                url: 'https://api.box.com/oauth2/token',
                data: body,
                headers: {
                    'Content-Type': 'application/json',
                },
                async: false,
                success: function(response) {
            		console.log(response)
                    //localStorage.setItem("token", response.access_token);
                    var putParam={
                        app:117,
                        id:resp.record["$id"]["value"],
                        record:{
                            refresh_token:{
                                value:response.refresh_token
                            },
                            access_token:{
                                value:response.access_token
                            }
                        },
                        __REQUEST_TOKEN__: kintone.getRequestToken()
                    }
                    var xhr = new XMLHttpRequest();
                    var url=kintone.api.url("/k/v1/record");
                    xhr.open('PUT', url,false);
                    xhr.setRequestHeader('X-Cybozu-API-Token', "4Jvm6tGkTBUGJKbANliQHgT4zuiHTlzjV5dg78uf");
                    xhr.setRequestHeader('Content-Type', 'application/json');
                    xhr.send(JSON.stringify(putParam));
                    if(xhr.status === 200){
                       token=response.access_token;
                    }else{
                        return null;
                    }
                },
                error: function(error) {
            		console.log(error);
            	}
            });
        }else{
            return null;
        }
        return token;
    }
	function myCheck() {
		var ret = "";
		var checkCnt = 0; // 選択されているか否かを判定する変数
		//var checkboxes = document.getElementsByName('radio');
		var checkboxes = document.getElementsByName('checkbox');
		var checkBoxValue = '';
		for (var i = 0; i < checkboxes.length; i++) {
			if (checkboxes[i].checked) {
				var val = checkboxes[i].value;
				if (ret !== "") ret += ",";
				ret += val;
				checkCnt = checkCnt + 1;
				//checkBoxValue = val;
				checkBoxValue = ret;
			}
		}
		// 何も選択されていない場合の処理 
		if (checkCnt === 0) {
			alert("選択されていません");
			return;
		} else if (checkCnt > 1) {
			alert("複数選択されています");
			return;
		}
		return checkBoxValue;
	}

	function addattachEvent(elem, event, func, useCapture) {
		if (elem.addEventListener) {
			elem.addEventListener(event, func, useCapture);
		} else if (elem.attachEvent) {
			elem.attachEvent('on' + event, func);
		}
	}

	/* 起動時のアクションを追加 */
	function windowEvent(event, func, useCapture) {
		if (window.addEventListener) {
			window.addEventListener(event, func, useCapture);
		} else if (window.attachEvent) {
			window.attachEvent('on' + event, func);
		}
	}

    kintone.events.on("app.record.index.show",function(event){
        /*var url = location.href;
        var match=url.split("?code=");
        if(match.length>1){
            var code=match[1];
            var body={
                grant_type:"authorization_code",
                code:code,
                client_id:CLIENT_CODE,
                client_secret:CLIENT_SECRET
            }
            $.ajax({
              type: 'POST',
              url: 'https://api.box.com/oauth2/token',
              data: body,
              headers: {
                'Content-Type': 'application/json',
              },
              async: false,
              success: function(response) {
        		  console.log(response)
                  localStorage.setItem("token", response.access_token);
                  localStorage.setItem("time",new Date());
                  console.log(localStorage.getItem("token"))
        		},
        		error: function(error) {
        			console.log(error);
        		}
            })
        }else{
            var updateTime=localStorage.getItem("time");
            //if(!updateTime || moment(updateTime).isAfter(moment())){
                location.href=URL;
            //}
        }*/
    })
	kintone.events.on("app.record.detail.show", function (event) {

		var record = event.record;
		//var status = record['txt_statusCode']['value'];
		var jobClient = record['txt_clientName']['value'];
        var boxBtn=document.createElement("button");
        boxBtn.id="box";
        boxBtn.innerHTML="フォルダ作成";
        kintone.app.record.getHeaderMenuSpaceElement().appendChild(boxBtn);
        boxBtn.onclick=function(){
            var folderName=record["txt_Folder"]["value"];
            var body={
                name:folderName,
                parent:{
                    id:"47513850899"
                }
            }
            var access_token=getAccessToken();
            console.log(access_token)
            if(access_token){
                var header={
                    'Content-Type': 'application/json',
                    "Authorization": "Bearer " + access_token
                }
                kintone.proxy('https://api.box.com/2.0/folders', 
                'POST',header,body, function(body, status, headers) {
                    //success
                    console.log(status, body, headers);
                }, function(error) {
                    //error
                    console.log(error);  //proxy APIのレスポンスボディ(文字列)を表示
                });
            }
        }
		var stasusArray = [];
		var appId = event.appId;
		var recId = event.recordId;
		//プロセスボタンスペース
		var spaceModal = kintone.app.record.getSpaceElement('sp_clientModal');
		var mySpaceFieldButton = kintone.app.record.getSpaceElement('btn_process');
		var mySpaceFieldModal = kintone.app.record.getSpaceElement('btn_modal');

		//var mySpaceFieldOutput =kintone.app.record.getSpaceElement('sp_outputButton');
		var buttonExcel = document.createElement('button');

		buttonExcel.id = "outPut";
		buttonExcel.innerHTML = '<font size="4">帳票出力</font>';
		buttonExcel.className = "btn";
		buttonExcel.style.background = "#FF4500"; //色は仮
		buttonExcel.style.color = "white"
		buttonExcel.style.height = "40px";
		buttonExcel.style.width = "100px";

		var select1 = document.createElement("select");
		var option = document.createElement('option');
		option.setAttribute('value', '1');
		option.innerHTML = "企業情報";

		select1.appendChild(option);

		//mySpaceFieldOutput.appendChild(buttonExcel);
		kintone.app.record.getHeaderMenuSpaceElement().appendChild(select1);
		kintone.app.record.getHeaderMenuSpaceElement().appendChild(buttonExcel);

		buttonExcel.onclick = function () {
			//ボタンを押したときの動作はここに書く
			alert("帳票出力ボタンクリック");
		};

		var $button1 = $('<button title="案件を登録する" class="kintoneplugin-button-dialog-ok">案件登録</button>');
		//モーダル表示
		var modalHTMLbefore = '<div class="modal fade" id="sampleModal" tabindex="-1">' +
			'<div class="modal-dialog">' +
			//'<div class="modal-dialog modal-content">'
			'<div class="modal-content"  style="width:740px; margin-left: -70px;">' +
			'<div class="modal-header">' +
			'<button type="button" class="close" data-dismiss="modal"><span>×</span></button>' +
			'<h4 class="modal-title">求職者選択</h4>' +
			'</div>' +
			'<div class="modal-body" style="max-height:' + (window.innerHeight - 213) + 'px;overflow:auto;">' +
			'<table class="kintoneplugin-table" id="tableId">' +
			'<thead>' +
			'<tr>' +
			'<th class="kintoneplugin-table-th" width="3%"><span class="title">選択</span></th>' +
			'<th class="kintoneplugin-table-th" width="25%"><span class="title">求職者名</span></th>' +
			'<th class="kintoneplugin-table-th"><span class="title">住所</span></th>' +
			'<th class="kintoneplugin-table-th-blankspace" width="3%"></th>' +
			'</tr>' +
			'</thead>' +
			'<tbody>'
		var modalHTMLafter = '</tbody>' +
			'</table>' +
			'</div>' +
			'<div class="modal-footer">' +
			'<button type="button" class="kintoneplugin-button-normal" data-dismiss="modal">キャンセル</button>' +
			'　' +
			'<button type="button" class="kintoneplugin-button-dialog-ok" id="getDataButton">登録</button>' +
			'</div>' +
			'</div>' +
			'</div>' +
			'</div>';

		var htmlContent = '';



		$button1.click(function () {

			htmlContent = '';

			Promise.resolve().then(function () {
				return new Promise(function (resolve, reject) {
					var str_query = '';
					var param = {};
					//顧客マスタから必要な情報を取得する
					//str_query = 'txt_num="' + custNo + '" and org_division in("'+event.record["組織選択"]["value"][0]["code"] +'")';
					str_query = 'order by $id asc';
					param = {
						app: APPID_APPLICANT,
						query: str_query,
						fields: ['$id', 'txt_jobApplicantId', 'txt_jobApplicantName', 'num_jobApplicantPostCode', 'txt_jobApplicantAdd'],
						totalCount: true,
						isGuest: false
					};

					kintoneUtility.rest.getAllRecordsByQuery(param).then(function (resp) {
						//success
						console.log(JSON.stringify(resp, null, '  '));

						if (resp.records.length !== 0) {
							//成功処理
							for (var i = 0; i < resp.records.length; i++) {
								var custName = resp.records[i]['txt_jobApplicantName']['value'];
								var custNo = resp.records[i]['txt_jobApplicantId']['value'];
								var pCode = resp.records[i]['num_jobApplicantPostCode']['value'];
								var add = resp.records[i]['txt_jobApplicantAdd']['value'];
								console.log(add);
								var pCodeBefore = pCode.slice(0, 3); //"182"
								var pCodeAfter = pCode.slice(-4); //"0004"

								//sex = resp.records[i]['rdo_Sex']['value'];
								//birthDay = resp.records[i]['date_BirthDay']['value'];
								//pCodeBefore = pCode.slice(0, 3); //"182"
								//pCodeAfter = pCode.slice(-4); //"0004"
								//momentBirthDay = moment(birthDay); //momentに変換
								//formatBD = momentBirthDay.format("YYYY年MM月DD日");
								//year = momentBirthDay.year(); //現在の年
								//month = momentBirthDay.month();
								//date = momentBirthDay.date();
								//誕生日
								//birth = new Date(year, month, date); // ここに誕生日を書いてください
								//y2 = paddingZero(birth.getFullYear(), 4);
								//m2 = paddingZero(birth.getMonth() + 1, 2);
								//d2 = paddingZero(birth.getDate(), 2);
								//if (logflg) console.log(y2 + '/' + m2 + '/' + d2);
								//age = Math.floor((Number(y1 + m1 + d1) - Number(y2 + m2 + d2)) / 10000);
								//ここでHTMLを作成
								htmlContent = htmlContent + '<tr><td><div class="kintoneplugin-table-td-control">'
								htmlContent = htmlContent + '<div class="kintoneplugin-table-td-control-value"><div class="kintoneplugin-input-outer">'
								htmlContent = htmlContent + '<div class="kintoneplugin-input-checkbox">'
								htmlContent = htmlContent + '<span class="kintoneplugin-input-checkbox-item">'
								//htmlContent = htmlContent + '<div class="kintoneplugin-input-radio"><span class="kintoneplugin-input-radio-item">'
								htmlContent = htmlContent + '<input type="checkbox" name="checkbox" value="'
								//htmlContent = htmlContent + '<input type="radio" name="radio" value="'
								htmlContent = htmlContent + custName
								//htmlContent = htmlContent + '" id="radio-' + i + '">'
								htmlContent = htmlContent + '" id="checkbox-' + i + '">'
								//htmlContent = htmlContent + '<label for="radio-' + i + '"></label></span></div>'
								htmlContent = htmlContent + '<label for="checkbox-' + i + '"></label></span></div>'
								htmlContent = htmlContent + '</div></div></div></td><td>'
								htmlContent = htmlContent + '<div class="kintoneplugin-table-td-control">'
								htmlContent = htmlContent + '<div class="kintoneplugin-table-td-control-value">'
								htmlContent = htmlContent + custName
								//htmlContent =+ '[' + sex + ']' + '<br>' + formatBD + '生[' + age + '歳]' 
								htmlContent = htmlContent + '</div></div></td><td><div class="kintoneplugin-table-td-control">'
								htmlContent = htmlContent + '<div class="kintoneplugin-table-td-control-value">'
								htmlContent = htmlContent + '〒' + pCodeBefore + '-' + pCodeAfter + '<br>' + add + '</div></div>'
								htmlContent = htmlContent + '</td></tr>';
							}
						}

						resolve();
					}).catch(function (error) {
						console.log(error.message);
						resolve();
					});

				}).then(function () {
					return new Promise(function (resolve, reject) {
						var modalHTML = modalHTMLbefore + htmlContent + modalHTMLafter;
						console.log(modalHTML);
						//console.log(modalHTML);
						$(spaceModal).html(modalHTML);
						/*
												$('#sampleModal').modal({
													//backdrop: "static"
													backdrop: "false"
												});
												*/

						$('#sampleModal').modal();

						$("#getDataButton").click(function () {
							var chkValue = '';
							chkValue = myCheck().split(",");
							//if (logflg) console.log(chkValue);
							if (chkValue.length == 1) {
								$('#sampleModal').modal('hide');
								spaceModal.innerHTML = "";


								//新規作成画面のURLへのジャンプ
								var new_window = window.open("/k/" + APPID_ANKEN + "/edit");
								addattachEvent(new_window, 'load', function () {
									window.postMessage(new_window.kintone !== null, location.origin);
								}, false);
								windowEvent('message', (function () {
									//window.addEventListener("message", (function () {
									return function field_set() {
										//キャンセルボタンが効かない対策 
										//参考：https://developer.cybozu.io/hc/ja/community/posts/115000486103
										var cancel1 = new_window.document.getElementsByClassName('gaia-ui-actionmenu-cancel');
										addattachEvent(cancel1[0], 'click', function () {
											new_window.close();
										}, false);
										//新規レコード側のフィールドを指定してsetする
										var new_app = new_window.kintone;
										var new_record = new_app.app.record.get();
										new_record['record']['txt_clientName']['value'] = jobClient;
										new_record['record']['txt_jobApplicantName']['value'] = chkValue[0];
										//new_record['record']['txt_address']['value'] = setFamilyAdd;
										//new_record['record']['num_PostalCode']['value'] = setFamilyPostCode;
										/*
										//編集不可
										new_record['record']['rdo_Frg']['value'] = '個人情報';
										new_record['record']['rdo_Household']['value'] = '世帯あり';
										new_record['record']['rdo_Frg']['disabled'] = true;
										new_record['record']['rdo_Household']['disabled'] = true;
										*/
										//ここから新規で開いたkintone画面でルックアップ先の更新処理を行う
										//new_record.record.company.lookup = true;
										new_app.app.record.set(new_record);
										window.removeEventListener("message", field_set, false);
									};
								})(), false);




								/*
								var get_record = kintone.app.record.get();
								get_record['record']['txt_Subcontractor']['value'] = chkValue[0];
								kintone.app.record.set(get_record);
								*/

								//return event;
							}
						})

					});
				})
			});
		});
		//if (!document.getElementById("modalButton")) {

		$(mySpaceFieldModal).append($button1);
		//return event;
		//}


	});
})(jQuery);